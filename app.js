const express = require("express"),
  bodyParser = require("body-parser"),
  http = require("http");

let app = express(),
  router = express.Router();


app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

app.use(function(req,res,next) {

  console.log(req.method, req.url);
  console.log("Origin", req.headers['x-forwarded-for'] || req.connection.remoteAddress);
  console.log("------------ BODY -------------");
  console.log(req.body);
  console.log("-------------------------------");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});


router.route("/oauth").get(function (req, res) {
  return res.status(200).send("Im alive!");
});

app.use("/api", router);


var server = http.createServer(app);

server.listen(8086, function () {
  console.log('Oauth2 Service is running!');
  console.log('visit => http://localhost:8086');
});
