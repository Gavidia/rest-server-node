const express = require('express')
const app = express()
const { authenticateRequest } = require('../middlewares/auth')



app.get('/', authenticateRequest, function(req, res) {

	res.send('Felicidades!');
});

module.exports = app;