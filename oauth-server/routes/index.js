const express = require('express');
const app = express();

app.use(require('./oauth'));
app.use(require('./resources'));

module.exports = app;