const mongoose = require('mongoose');

let Schema = mongoose.Schema;


let tokenShema = new Schema({

    accessToken: {
        type: String
    },
    accessTokenExpiresAt: {
        type: Date
    },
    refreshToken: {
        type: String
    },
    refreshTokenExpiresAt: {
        type: Date
    },
    client: {
        type: Object
    },
    user: Object
});

module.exports = mongoose.model('Token',tokenShema);