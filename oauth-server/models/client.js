const mongoose = require('mongoose')

let Schema = mongoose.Schema;

let clientSchema = new Schema({

    clientId: {
        type: String
    },
    clientSecret: {
        type: String
    },
    grants: {
        type: [String]
    },
    redirectUris: {
        type: [String]
    }

});

module.exports = mongoose.model('Client',clientSchema);