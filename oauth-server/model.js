const mongoose = require('mongoose');


const Client = require('./models/client');
const User = require('./models/user');
const Token = require('./models/token');

module.exports = {
  getAccessToken,
  getClient,
  saveToken,
  getUser,
  getUserFromClient
};
//==========================================================================
function loadExampleData () {

  let client1 = new Client({
    clientId: 'application',
    clientSecret: 'secret',
    grants: [
      'password'
    ],
    redirectUris: []
  });

  let client2 = new Client({
    clientId: 'confidentialApplication',
    clientSecret: 'topSecret',
    grants: [
      'password',
      'client_credentials'
    ],
    redirectUris: []
  });

  let user = new User({
    username: 'Diango',
    password: 'password'
  });

  client1.save(function (err, client) {

    if (err) {
      return console.error(err);
    }
    console.log('Created client', client);
  });

  user.save(function (err, user) {

    if (err) {
      return console.error(err);
    }
    console.log('Created user', user);
  });

  client2.save(function (err, client) {

    if (err) {
      return console.error(err);
    }
    console.log('Created client', client);
  });
};

/**
 * Dump the database content (for debug).
 */

function dump () {

  Client.find(function (err, clients) {

    if (err) {
      return console.error(err);
    }
    console.log('clients', clients);
  });

  Token.find(function (err, tokens) {

    if (err) {
      return console.error(err);
    }
    console.log('tokens', tokens);
  });

  User.find(function (err, users) {

    if (err) {
      return console.error(err);
    }
    console.log('users', users);
  });
};

//==========================================================================

function getAccessToken (token) {

  return Token.findOne({
    accessToken: token
  });
};

function getClient (clientId, clientSecret) {

  return Client.findOne({
    clientId,
    clientSecret
  });
};

function saveToken (token, client, user) {

  token.client = {
    id: client.clientId
  };

  token.user = {
    id: user.username || user.clientId
  };

  let tokenInstance = new Token(token);

  tokenInstance.save();

  return token;
};

function getUser (username, password) {

  return User.findOne({
    username,
    password
  });
};

function getUserFromClient (client) {

  return Client.findOne({
    clientId: client.clientId,
    clientSecret: client.clientSecret,
    grants: 'client_credentials'
  });
};