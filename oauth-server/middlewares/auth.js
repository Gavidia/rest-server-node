const express = require('express');
const app = express();
const OAuth2Server = require('oauth2-server');
const Request = OAuth2Server.Request;
const Response = OAuth2Server.Response;

// OAuth
app.oauth = new OAuth2Server({
    model: require('../model'),
    accessTokenLifetime: 60*60,
    allowBearerTokensInQueryString: true
});

let authenticateRequest = (req, res, next) => {

	var request = new Request(req);
	var response = new Response(res);

	return app.oauth.authenticate(request, response)
		.then(function(token) {

			next();
		}).catch(function(err) {

			res.status(err.code || 500).json(err);
		});
}

module.exports = {
    authenticateRequest
}