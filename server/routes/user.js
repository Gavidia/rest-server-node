const express = require('express')
const bcrypt = require('bcrypt-nodejs')
const _ = require('underscore')

const User = require('../models/user')
const { authToken } = require('../middlewares/auth')

const app = express()

  app.get('/user', authToken, (req, res) => {

    let from = req.query.from || 0;
    from = Number(from);

    let limit = req.query.limit || 5;
    limit = Number(limit);

    User.find({ state: true }, 'nombre email role state google img')
            .skip(from)
            .limit(limit)
            .exec( (err, users ) => {

                if(err){
                    return res.status(400).json({
                        ok: false,
                        err
                    })
                }

                User.count({ state: true }, (err,count) => {
                    res.json({
                        ok: true,
                        users,
                        count
                    });
                })
            });

  })
  
  app.post('/user', authToken, (req, res) => {
  
      let body = req.body;
      
      body.password =  bcrypt.hashSync(body.password);

      let user = new User({
          name: body.name,
          email: body.email,
          password: body.password,
          role: body.role
      });

      user.save((err, userDB) => {

        if(err){
            return res.status(400).json({
                ok: false,
                err
            });
        }

        res.json({
            ok: true,
            user: userDB
        })
      })

  })
  
  app.put('/user/:id', authToken, (req, res) => {

      let id = req.params.id;
      let body = _.pick(req.body, ['nombre','email','img','role','state']) ;

      User.findByIdAndUpdate(id, body, {new: true, runValidators: true}, (err,userDB) => {

        if(err){
            return res.status(400).json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            user: userDB
        })
          
      })
  })
  
  app.delete('/user/:id', authToken, (req, res) => {
        
      let id = req.params.id;
      
      let changeState = {
        state: false
      }
      User.findByIdAndUpdate(id, changeState, { new: true }, (err,userDB) => {

        if(err){
            return res.status(400).json({
                ok: false,
                err
            })
        }

        res.json({
            ok: true,
            user: userDB
        })

      })

  })

  module.exports = app;