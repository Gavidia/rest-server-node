# OAuth2-Server

## Fuentes:

- https://oauth2-server.readthedocs.io/en/latest/
- https://github.com/pedroetb/node-oauth2-server-mongo-example
- https://oauth.net/2/

# OAuth 2


## Grant_types

En OAuth 2.0, el término "grant type" se refiere a la forma en que una aplicación obtiene un token de acceso. OAuth 2.0 define varios tipos de grant types, incluyendo la concesión de Contraseña. Las extensiones de OAuth 2.0 también pueden definir nuevos tipos de subvenciones.

### password

El OAuth 2.0 Grant Type Password  es una forma de obtener un token de acceso con un nombre de usuario y una contraseña. Por lo general, sólo lo utilizan las aplicaciones móviles de un servicio y no se pone a disposición de los desarrolladores de terceros.

El Grant Type Password es uno de los más simples de OAuth e implica sólo un paso: la aplicación presenta un formulario tradicional de inicio de sesión de nombre de usuario y contraseña para recopilar las credenciales del usuario y realiza una solicitud POST al servidor para intercambiar la contraseña por un token de acceso. La solicitud POST que hace la aplicación se parece al ejemplo de abajo.

```http
POST /oauth/token HTTP/1.1
Host: authorization-server.com
Content-type: application/x-www-form-urlencoded

grant_type=password
&username=exampleuser
&password=1234luggage
&client_id=xxxxxxxxxx
```

- **grant_type** = password 
   Esto le dice al servidor que estamos usando el tipo de concesión de contraseña.

- **username** 
  El nombre de usuario del usuario que introdujo en la aplicación
  password= - La contraseña del usuario que ha introducido en la aplicación.

- **client_id** 
  El identificador público de la aplicación que el desarrollador obtuvo durante el registro.

- **client_secret** 
  (opcional)
  Si la aplicación es un "cliente confidencial" (no una aplicación móvil o JavaScript), también se incluye el secret.

- **scope** 
  (opcional) 
  Si la aplicación está solicitando un token de alcance limitado, debe proporcionar aquí los alcances solicitados.
  El servidor responde con un token de acceso en el mismo formato que los otros tipos de grant types.

### Demo usando la librería oauth2-server

Se necesita tener en la base de datos:

**Cliente**

Por ejemplo:

``` 
clientId: application
secret: secret
```

**User**

Por ejemplo

```
username: Diango
password: password
```

#### Obteniendo un Token

Se debe hacer una petición post a `http://localhost:8086/oauth/token` con los siguientes Headers

```
Authorization: "Basic " + clientId:secret base64'd
Content-Type: application/x-www-form-urlencoded
```

Ejemplo codificando clientid:secret con
`[Base64 Codificar - Online Base64 Encoder](http://www.convertstring.com/es/EncodeDecode/Base64Encode)`
```
//Headers
Authorization: Basic YXBwbGljYXRpb246c2VjcmV0
Content-Type: application/x-www-form-urlencoded

//Body
grant_type=password
username=Diango
password=password
```

La respuesta, si todo fue correcto, debe ser 
```json
{
    "accessToken": "fd2871b4f4cd2762b996c0cc43e87dbfb5a5c2cb",
    "accessTokenExpiresAt": "2019-01-31T13:14:05.263Z",
    "refreshToken": "35076b5e8ac0370dd63c3a93dbe5f0fc33b68abe",
    "refreshTokenExpiresAt": "2019-02-14T12:14:05.263Z",
    "client": {
        "id": "application"
    },
    "user": {
        "id": "Diango"
    }
}
```
#### Uso de un Access Token para acceder a la información

En este proyecto, la información se encuentra en `localhost:8086/`, por lo que se debe enviar una petición GET a esa dirección con el siguiente formato:

```
//Headers
Authorization: Bearer  fd2871b4f4cd2762b996c0cc43e87dbfb5a5c2cb

```

Si todo es correcto, el servidor devuelve:

`Felicidades!`

## Flujo del protocolo

RFC 6749                        OAuth 2.0                   October 2012

### 1.1.  Roles

   OAuth defines four roles:

   resource owner
​      An entity capable of granting access to a protected resource.
​      When the resource owner is a person, it is referred to as an
​      end-user.

   resource server
​      The server hosting the protected resources, capable of accepting
​      and responding to protected resource requests using access tokens.

   client
​      An application making protected resource requests on behalf of the
​      resource owner and with its authorization.  The term "client" does
​      not imply any particular implementation characteristics (e.g.,
​      whether the application executes on a server, a desktop, or other
​      devices).

   authorization server
​      The server issuing access tokens to the client after successfully
​      authenticating the resource owner and obtaining authorization.

   The interaction between the authorization server and resource server
   is beyond the scope of this specification.  The authorization server
   may be the same server as the resource server or a separate entity.
   A single authorization server may issue access tokens accepted by
   multiple resource servers.

### 1.2.  Protocol Flow

     +--------+                               +---------------+
     |        |--(A)- Authorization Request ->|   Resource    |
     |        |                               |     Owner     |
     |        |<-(B)-- Authorization Grant ---|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(C)-- Authorization Grant -->| Authorization |
     | Client |                               |     Server    |
     |        |<-(D)----- Access Token -------|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(E)----- Access Token ------>|    Resource   |
     |        |                               |     Server    |
     |        |<-(F)--- Protected Resource ---|               |
     +--------+                               +---------------+
    
                     Figure 1: Abstract Protocol Flow

   The abstract OAuth 2.0 flow illustrated in Figure 1 describes the
   interaction between the four roles and includes the following steps:

   (A)  The client requests authorization from the resource owner.  The
​        authorization request can be made directly to the resource owner
​        (as shown), or preferably indirectly via the authorization
​        server as an intermediary.

   (B)  The client receives an authorization grant, which is a
​        credential representing the resource owner's authorization,
​        expressed using one of four grant types defined in this
​        specification or using an extension grant type.  The
​        authorization grant type depends on the method used by the
​        client to request authorization and the types supported by the
​        authorization server.

   (C)  The client requests an access token by authenticating with the
​        authorization server and presenting the authorization grant.

   (D)  The authorization server authenticates the client and validates
​        the authorization grant, and if valid, issues an access token.

   (E)  The client requests the protected resource from the resource
​        server and authenticates by presenting the access token.

   (F)  The resource server validates the access token, and if valid,
​        serves the request.

    The preferred method for the client to obtain an authorization grant
    from the resource owner (depicted in steps (A) and (B)) is to use the
    authorization server as an intermediary, which is illustrated in
    Figure 3 in Section 4.1.
    
    ---
### 4.1.  Authorization Code Grant

   The authorization code grant type is used to obtain both access
   tokens and refresh tokens and is optimized for confidential clients.
   Since this is a redirection-based flow, the client must be capable of
   interacting with the resource owner's user-agent (typically a web
   browser) and capable of receiving incoming requests (via redirection)
   from the authorization server.

     +----------+
     | Resource |
     |   Owner  |
     |          |
     +----------+
          ^
          |
         (B)
     +----|-----+          Client Identifier      +---------------+
     |         -+----(A)-- & Redirection URI ---->|               |
     |  User-   |                                 | Authorization |
     |  Agent  -+----(B)-- User authenticates --->|     Server    |
     |          |                                 |               |
     |         -+----(C)-- Authorization Code ---<|               |
     +-|----|---+                                 +---------------+
       |    |                                         ^      v
      (A)  (C)                                        |      |
       |    |                                         |      |
       ^    v                                         |      |
     +---------+                                      |      |
     |         |>---(D)-- Authorization Code ---------'      |
     |  Client |          & Redirection URI                  |
     |         |                                             |
     |         |<---(E)----- Access Token -------------------'
     +---------+       (w/ Optional Refresh Token)

   Note: The lines illustrating steps (A), (B), and (C) are broken into
   two parts as they pass through the user-agent.

                     Figure 3: Authorization Code Flow

The flow illustrated in Figure 3 includes the following steps:

   (A)  The client initiates the flow by directing the resource owner's
​        user-agent to the authorization endpoint.  The client includes
​        its client identifier, requested scope, local state, and a
​        redirection URI to which the authorization server will send the
​        user-agent back once access is granted (or denied).

   (B)  The authorization server authenticates the resource owner (via
​        the user-agent) and establishes whether the resource owner
​        grants or denies the client's access request.

   (C)  Assuming the resource owner grants access, the authorization
​        server redirects the user-agent back to the client using the
​        redirection URI provided earlier (in the request or during
​        client registration).  The redirection URI includes an
​        authorization code and any local state provided by the client
​        earlier.

   (D)  The client requests an access token from the authorization
​        server's token endpoint by including the authorization code
​        received in the previous step.  When making the request, the
​        client authenticates with the authorization server.  The client
​        includes the redirection URI used to obtain the authorization
​        code for verification.

   (E)  The authorization server authenticates the client, validates the
​        authorization code, and ensures that the redirection URI
​        received matches the URI used to redirect the client in
​        step (C).  If valid, the authorization server responds back with
​        an access token and, optionally, a refresh token.

### 1.5.  Refresh Token

   Refresh tokens are credentials used to obtain access tokens.  Refresh
   tokens are issued to the client by the authorization server and are
   used to obtain a new access token when the current access token
   becomes invalid or expires, or to obtain additional access tokens
   with identical or narrower scope (access tokens may have a shorter
   lifetime and fewer permissions than authorized by the resource
   owner).  Issuing a refresh token is optional at the discretion of the
   authorization server.  If the authorization server issues a refresh
   token, it is included when issuing an access token (i.e., step (D) in
   Figure 1).

   A refresh token is a string representing the authorization granted to
   the client by the resource owner.  The string is usually opaque to
   the client.  The token denotes an identifier used to retrieve the 
   authorization information.  Unlike access tokens, refresh tokens are
   intended for use only with authorization servers and are never sent
   to resource servers.
```
  +--------+                                           +---------------+
  |        |--(A)------- Authorization Grant --------->|               |
  |        |                                           |               |
  |        |<-(B)----------- Access Token -------------|               |
  |        |               & Refresh Token             |               |
  |        |                                           |               |
  |        |                            +----------+   |               |
  |        |--(C)---- Access Token ---->|          |   |               |
  |        |                            |          |   |               |
  |        |<-(D)- Protected Resource --| Resource |   | Authorization |
  | Client |                            |  Server  |   |     Server    |
  |        |--(E)---- Access Token ---->|          |   |               |
  |        |                            |          |   |               |
  |        |<-(F)- Invalid Token Error -|          |   |               |
  |        |                            +----------+   |               |
  |        |                                           |               |
  |        |--(G)----------- Refresh Token ----------->|               |
  |        |                                           |               |
  |        |<-(H)----------- Access Token -------------|               |
  +--------+           & Optional Refresh Token        +---------------+

               Figure 2: Refreshing an Expired Access Token
```

   The flow illustrated in Figure 2 includes the following steps:

   (A)  The client requests an access token by authenticating with the
​        authorization server and presenting an authorization grant.

   (B)  The authorization server authenticates the client and validates
​        the authorization grant, and if valid, issues an access token
​        and a refresh token.

   (C)  The client makes a protected resource request to the resource
​        server by presenting the access token.

   (D)  The resource server validates the access token, and if valid,
​        serves the request.

   (E)  Steps (C) and (D) repeat until the access token expires.  If the
​        client knows the access token expired, it skips to step (G);
​        otherwise, it makes another protected resource request.

   (F)  Since the access token is invalid, the resource server returns
​        an invalid token error.

   (G)  The client requests a new access token by authenticating with
​        the authorization server and presenting the refresh token.  The
​        client authentication requirements are based on the client type
​        and on the authorization server policies.

   (H)  The authorization server authenticates the client and validates
​        the refresh token, and if valid, issues a new access token (and,
​        optionally, a new refresh token).

   Steps (C), (D), (E), and (F) are outside the scope of this
   specification, as described in Section 7.